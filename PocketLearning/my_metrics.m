function [metrics]=my_metrics(desired_test_result,simulated_test_out)

%TP true positive, t1 classified as t1
%TN true negative, non-t1 (t2 and t3) classified as non-t1
%FP false positive, non-t1 classified as t1
%FN false negative, t1 classified as non-t1
%use binary form...
%add desired output test response and the network simulation results
%i.e. desired   1 1 1 0 0 0 (binary case)
%i.e. response  1 0 1 0 0 1
%add            2 1 2 0 0 1
%see that;
%number of 2's TP
%number of 0's TN
%subtract       0 1 0 0 0 -1
%number of -1's FP
%number of 1's FN

%Copyright (c) 2016 positronics.engineer at yandex dot com.
%Distributed under the terms of the MIT license.

sum_out=desired_test_result+simulated_test_out;
sub_out=desired_test_result-simulated_test_out;

TP=sum(double([sum_out==2]),2);
TN=sum(double([sum_out==0]),2);
FP=sum(double([sub_out==-1]),2);
FN=sum(double([sub_out==1]),2);

%CC correct classification, Accuracy also
%SE Sensitivity, recall also
%SP Specificity,
%PPV Positive predictive value, selectivity,precision also
%NPV Negative predictive value
%http://dms.irb.hr/tutorial/tut_mod_eval_1.php
N=TP+TN+FP+FN;
per_CC=100*(TP+TN)./N;
per_FP=100*FP./(TN+TP);
per_FN=100*FN./(TP+FN);
per_SE=100*TP./(TP+FN);
per_SP=100*TN./(TN+FP);
per_PPV=100*TP./(TP+FP);
per_NPV=100*TN./(TN+FN);

metrics=[per_CC, per_FP,per_FN,per_SE,per_SP,per_PPV,per_NPV];