function [results]=my_percep_test(w,test)
%[results]=my_percep_test(weight,test)
%w comes from the result of training, column vector
%test is the test pattern, in columnwise
%results is the output of the perceptron

%Copyright (c) 2016 positronics.engineer at yandex dot com.
%Distributed under the terms of the MIT license.

[m,n]=size(test);
bias=ones(1,n);
test=[bias;test];
results=[];
for i=1:n
    results(i)=hardlim(w'*test(:,i));
end