function [poc_w,err,w]=my_percep_pocketlearn(x,d,iter)
%[poc_w,e,w]=my_percep_pocketlearn(x,d,iter)
%x is the input vector, features are in column-wise
%d is the target vector, in binary form
%iter is the number of iterations, epochs
%returns poc_w (pocketed weights)
%err is the total mean square error at the end of a epoch
%w is the result of usual perceptron learning algorithm

%Copyright (c) 2016 positronics.engineer at yandex dot com.
%Distributed under the terms of the MIT license.

[m,n]=size(x);
bias=ones(1,n);
x=[bias;x];%add bias component to each training pattern
m=m+1;%since bias adds one more dimensions increase it
w=randn(m,1);%initial weight is selected randomly
poc_w=zeros(m,1);%initialise pocketed weight vector
run=0;%count the number of true decisions during training
run_max=0;%number of max true decision made by perceptron, used to decide whether pocket the weight
err=[];

for epoch=1:iter
    modif=0;%count the number of modification on weights at each epoch
    index=randperm(n); %select the input patterns randomly from the given training set
    random_inputs=x(:,index);%new order of training patterns
    random_targets=d(index);
%     random_inputs=x;
%     random_targets=d;
    e=[];
    for i=1:n
        y=hardlim(w'*random_inputs(:,i));
        e(i)=(y-random_targets(i));
        if isequal(y,random_targets(i))
            run=run+1;
            if run>run_max
                run_max=run;
                poc_w=w;
            end
        else
            modif=modif+1;
            run=0;
            if (y==1)&&(random_targets(i)==0)
                w=w-random_inputs(:,i);
            else
                w=w+random_inputs(:,i);
            end
        end
    end
    err(epoch)=sum(e.^2)/n;
    if modif==0%break if no modifications on weights, convergence
        break;
    end
end