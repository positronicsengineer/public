//Copyright (c) 2016 positronics.engineer at yandex dot com.
//Distributed under the terms of the MIT license.

#include <stdio.h>
#include <stdlib.h>
#include "myextern_array.h"

/**************************************************************/
/* infile is the name of the file to be imported from file system
** row_num and col_num pointer modified to give array dimensions
** pointer to array of pointers is returned, hence you can use array[i][j] notation for data call
** i,j starts from 0
** infile supposed to be save-as tab delimited txt from excel table
** format is as below
** [double_data][\t][double_data][\t]....[double_data][\n]
** [double_data][\t][double_data][\t]....[double_data][\n]
** ......
** [double_data][\t][double_data][\t]....[double_data][\n]
** check "example_extern_array.txt"
*/
double** import2Darray(const char *infile, double** array, int *row_num, int *col_num)
{
    char str[64], ch, *cptr;
    int i=0, col=0, row=0, r=0, c=0;
    FILE *fp;

    if((fp=fopen(infile,"r"))==NULL)
    {
        fprintf(stderr, "Couldn't open file '%s'\n",infile);
        exit(0);
    }

    //count tab (\t) and linefeed (\n) to define col and row number
    while(!feof(fp))
    {
        ch=fgetc(fp);
        if (ch=='\t')
            i++;
        else if(ch=='\n')
            row++;
    }
    fclose(fp); //close file and reopen later and point to file beginning

    col=i/row+1;

    //allocate dynamic continuous memory area for 2d double array
    array=allocate2D(array,row,col);
    if(array==NULL)
    {
        fprintf(stderr, "Couldn't allocate memory.\n");
        exit(0);
    }

    i=0;
    r=0;
    c=0;
    //reopen file to import each char and convert to double
    fp=fopen(infile,"r");
    while(!feof(fp))
    {
        ch=fgetc(fp);
        if (ch!='\n'||ch!='\t')
        {
            str[i]=ch;
            i++;
        }
        if(ch=='\t')
        {
            array[r][c]=strtod(str,&cptr);
            //printf("r=%d, c=%d, double data %f\n",r,c,array[r][c]);	//uncomment to debug
            c++;
            i=0;
        }
        if(ch=='\n')
        {
            array[r][c]=strtod(str,&cptr);
            //printf("r=%d, c=%d, double data %f\n",r,c,array[r][c]);	//uncomment to debug
            c=0;
            r++;
            i=0;
        }
    }

    *row_num=row;
    *col_num=col;
    return array;
}
/**************************************************************/


//Thanks to;
//https://gsamaras.wordpress.com/code/2d-dynamic-array-in-continuous-memory-locations-c/
//for more details on concept
//http://www.geeksforgeeks.org/dynamically-allocate-2d-array-c/
//allocate continuous memory area for 2d double array, dynamically
double** allocate2D(double** A, const int row, const int col)
{
    int i;
    double *t0;

    A = malloc(row * sizeof (double*)); /* Allocating pointers */
    t0 = malloc(row * col * sizeof (double)); /* Allocating data */
    for (i = 0; i < row; i++)
        A[i] = t0 + i * (col);

    return A;
}

//call before finishing main(), to free the occupied memory area
void free2Darray(double** p)
{
    free(p[0]);
    free(p);
}
