//Copyright (c) 2016 positronics.engineer at yandex dot com.
//Distributed under the terms of the MIT license.

#include <string.h>
#include "mymatlab.h"
#include "mat.h"	//MATLAB's mat.h


/**************************************************************/
/*This function saves the pointed real double data array as a MAT file
**dimensions are determined by rows and cols parameters
**name of the mat file is output_name and
**name of the variable inside the matfile is var_name
**it stores only one variable into a MAT file
**columns number is used as row number in the function
**and vice versa, row number is used as column number
**USE transpose of saved MAT file in MATLAB calculations!!!
**this is because MATLAB header assumes arrays in memory as columns order, but C creates in row order
**MATLAB 6.5 and greater
*/
void saveasMAT2(int rows,int cols,double *input,char *output_name,char *var_name)
{
 MATFile *outmat=NULL;
 mxArray *out=NULL;
 const char **returned_var_name=NULL;	//If MATLAB version 6.5 or greater
 //columns number is used as row number and vice versa
 //!!!USE TRANSPOSE of saved MAT file in MATLAB calculations!!!
 out=mxCreateDoubleMatrix(cols,rows,mxREAL);


 if(out==NULL)
	{
	  	printf("Real double matrix cannot be created and variable cannot be saved as MAT file\n");
	 	exit(-1);
	 }
 memcpy(mxGetPr(out), input, rows*cols*sizeof(double));
 outmat=matOpen(output_name, "w");
 if(outmat==NULL)
	{
	  	printf("MAT file cannot be opened and variable cannot be saved as MAT file\n");
	 	exit(1);
	 }
	matPutVariable(outmat,var_name, out);//if MATLAB version is 6.5 or greater

 if (matClose(outmat) != 0)
  	{
            	printf("Error closing file %s\n",output_name);
            	exit(1);
  	}

 //reopen to check if the file is created properly
 outmat = matOpen(output_name, "r");

 out = matGetNextVariable(outmat,returned_var_name); //if MATLAB version is 6.5 or greater
 //returned_var_name is the name of the MATLAB variable returned by the matGetNextVariable function

 if (out == NULL)
        {
      		    printf("Error reading in file %s\n", output_name);
      		    exit(1);
         }

 //close the file again
 if (matClose(outmat) != 0)
  	{
            printf("Error closing file %s\n",output_name);
            exit(1);
  	}
 mxDestroyArray(out);
}
/**************************************************************/

/**************************************************************/
/*this function used to import an MATLAB array into C
**it takes the mat file name and
**returns the pointer of the data which is in double format
**and also number of rows and columns are written to pointed area
** to prevent confusions of MATLAB and C array placing in memory
**(MATLAB places arrays in column order but C in row order)
**simply use transpose of MATLAB variable
**assuming only one variable in the mat file!
**MATLAB 6.5 and greater
*/

double *importMAT(char *file, int *rows_addr,int *cols_addr)
{
  MATFile *pmat=NULL;
  double *dataptr=NULL;
  mxArray *pa=NULL;

  const char *returned_var_name=NULL;//If MATLAB version 6.5 or greater

  //printf("Reading file '%s'.\n", file);

  /* Open file to get directory. */
  pmat = matOpen(file, "r");
  if (pmat == NULL)
  	{
    		printf("Error opening file %s\n", file);
    		exit(1) ;
  	}


  /* In order to use matGetNextXXX correctly, reopen file to read in headers. */
  if (matClose(pmat) != 0)
  	{
            printf("Error closing file %s\n",file);
            exit(1);
  	}

   pmat = matOpen(file, "r");


    pa = matGetNextVariable(pmat,&returned_var_name); //if MATLAB version is 6.5 or greater
  	//returned_var_name is the name of the MATLAB variable returned by the matGetNextVariable function

   if (pa == NULL)
        {
      		    printf("Error reading in file %s\n", file);
      		    exit(1);
         }

   dataptr=mxGetPr(pa);
   *rows_addr=mxGetM(pa);
   *cols_addr=mxGetN(pa);

   if (matClose(pmat) != 0)
  	{
    		printf("Error closing file %s\n",file);
    		exit(1);
  	}

   if(dataptr==NULL)
    {
            printf("Data pointer is NULL");
            exit(1);
     }
   printf("Variable name is '%s', rows = %d, cols= %d matrix.\n", returned_var_name, *rows_addr, *cols_addr);
   //printf("Array is imported.\n");
   return dataptr;
}

/**************************************************************/

/**************************************************************/
/*this function returns the offset of the (i,j)th data according to starting pointer
**of a 2 dimensional array, rows is number of rows, cols is number of columns
**in the array. (i,j) usage is similar to MATLAB's indexing.See below
**|(1,1) (1,2) (1,3) ... (.,.)|
**|(2,1) (2,2) (2,3) ... (.,.)|
**|.....  (i,j) ..............|
**|...........................|
**offset is offset data of (i,j)th data. (i,j) value should be in the position
** *(starting_point_of_the_array+offset)
*/
int convert2dim_indexes(int i,int j,int rows,int cols)
{
 int offset;
 return offset=(j-1)*rows+(i-1);
}
/*important note!!!
**MATLAB stores 2D arrays in memory as columns wise order, C stores row wise order
**consider matrix below
**0 1 2
**3 4 5
**when import this matrix from MATLAB's mat file, it is stored as column wise in memory
**|ptr[0] | ptr[1] | ptr[2] | ptr[3] | ptr[4] | ptr[5] |
**| 0 | 3 | 1 | 4 | 2 | 5 |
**if you create this matrix in C, it is stored as row wise
**|ptr[0] | ptr[1] | ptr[2] | ptr[3] | ptr[4] | ptr[5] |
**| 0 | 1 | 2 | 3 | 4 | 5 |
**example code, examine pointer addresses
    int array[2][3]={{0,1,2},{3,4,5}};
    int *p;
    for(i=0;i<2;i++)
        for (j=0;j<3;j++)
            printf("array[%d][%d] is %d, memory location (pointer) is %p\n",i,j,array[i][j],&(array[i][j]));
*/
/**************************************************************/


/**************************************************************/
/*this function gets the (i,j)th element of the MATLAB array
**dataptr shows the starting point of the array data
**rows and cols are dimensions of array
**use MATLAB's indexing, start at (1,1) (not (0,0) unlike C)
*/
double getdata(double *dataptr,int i,int j,int rows,int cols)
{
 double data;
 int offset;
 offset=convert2dim_indexes(i,j,rows,cols);
 return data=*(dataptr+offset);
}
/**************************************************************/
