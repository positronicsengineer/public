//Copyright (c) 2016 positronics.engineer at yandex dot com.
//Distributed under the terms of the MIT license.

#include <stdio.h>
#include <stdlib.h>

#include "mymatlab.h"
#include "myextern_array.h"

int main(int argc, char* argv[])
{
    int cols, rows, i, j;
    char *text_file=NULL, *mat_file=NULL, *out_mat_file=NULL, *ext_array=NULL, *out_mat_var=NULL;
    double *dataptr=NULL;
    FILE *outf=NULL;    //file output
    double **arr=NULL;

    if( argc != 6 )
    {
        fprintf( stderr, "Usage: matfile.exe <input_mat_file> <output_txt_file> <input_ext_array> <output_mat_file> <output_mat_var>\n" );
        fprintf(stderr, "Example code to demonstrate basic functions.\n");
        return 1;
    }
    mat_file=argv[1];
    text_file=argv[2];
    ext_array=argv[3];
    out_mat_file=argv[4];
    out_mat_var=argv[5];

    printf("call for importMAT\n");
    printf("------------------\n");
    dataptr=importMAT(mat_file,&rows,&cols);
    //writing data into a text file
    if((outf=fopen(text_file,"w"))==NULL)
    {
        fprintf(stderr, "Couldn't open file '%s'.\n",text_file);
        return 1;
    }

    for(i=1; i<=rows; i++)
    {
        for (j=1; j<=cols; j++)
        {
            fprintf(outf, "%f\t",getdata(dataptr,i,j,rows,cols));
            //getdata() assumes standard MATLAB indexing, starts from (1,1), unlike C (0,0)
        }

        fprintf(outf, "\n");
    }

    fclose(outf);
    printf("'%s' is exported as '%s'.\n\n", mat_file, text_file);
    //end writing text file

    printf("call for saveasMAT2\n");
    printf("-------------------\n");
    //saving as MAT files
    arr = import2Darray(ext_array,arr,&rows,&cols);
    printf("rows = %d, cols = %d,\n",rows,cols);
    printf("External array elements:\n");

    for (i=0; i<rows; i++)
    {
        for (j=0; j<cols; j++)
        {
            printf("%f\t",arr[i][j]);
        }
        printf("\n");
    }

    saveasMAT2(rows,cols,*arr,out_mat_file,out_mat_var);
    printf("USE TRANSPOSE of saved MAT file in MATLAB calculations!!!\n");
    //see function .c explanations for the reason
    printf("Saving external array as '%s'.\n", out_mat_file);
    printf("MATLAB variable name is '%s'.\n", out_mat_var);

    free2Darray(arr);
    printf("\nEnd of example operations.\n");

return 0;   /*END*/
}
