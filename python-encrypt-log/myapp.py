#!/usr/bin/python3

import logging
import encryptlog

#you can set logger based on a value from ini file, so it can be enabled when necessary
#if debugLog is true in ini file
encryptlog.setEncryptedLogger() #this will encrypt whole logs

#appLogger = encryptlog.setEncryptedLocalLogger(__name__) #this is to set locally encrypted logger

def run():
    logging.debug("Sensitive logs, from my app")
    a = 5
    logging.debug("Debug value: %d" % (a+1))
    return True
