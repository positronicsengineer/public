#!/usr/bin/python3

import myapp
import logging
logging.basicConfig(level=logging.DEBUG)

if __name__ == '__main__':
    logging.debug("This is debug log from main app")
    myapp.run()