#!/usr/bin/python3

#Copyright (c) 2022 positronics.engineer at yandex dot com.
#Distributed under the terms of the MIT license.

#decrypt log messages
#call setEncryptedLogger() to set encrypted logger, should be early call
#decrypt log by calling this script
#!!!!!IMPORTANT!!!!!
#do not distrubte this, just use in your local to decrypt logs

#reference
#this is with pycrypto which is obsolete
#https://stackoverflow.com/questions/44475816/python-logging-module-encrypt

import logging
from cryptography.fernet import Fernet
import os
import encryptlog

outfilesuffix = '.decrypt' #suffix to add decrypted log file
infilesuffix = '.txt'
naming = 'log'

def log_decryptor(stream, key):
    #assume the stream can be iterated line-by-line
    for line in stream:
        if not line.strip():  # empty line...
            continue  # ignore it!
        #split on log level and log data
        #encrypted log format like, defined in setEncryptedLogger()
        #27-06-2022 14:18:02 [main.py - <module> - 8] --- gAAAAABiuaqSUc04QM5Nem5hLCm
        level, encrpted_msg_body = line.split(" --- ", 1)
        f = Fernet(key)
        decrypted_msg_body = f.decrypt(encrpted_msg_body.encode("utf-8"))
        msg = decrypted_msg_body.decode("latin-1")
        yield ("%s --- %s" %(level, msg))
        
def logFile_decrypt(key):
    dirPath = os.path.dirname(os.path.realpath(__file__))
    for fileName in os.listdir(dirPath):
        if fileName.endswith(infilesuffix) and naming in fileName:
            file = open(os.path.join(dirPath, fileName), 'r')
            file_decrypted = open(os.path.join(dirPath, fileName.replace(infilesuffix,outfilesuffix)), 'w') #add suffix to file name
            for line in log_decryptor(file, key):
                file_decrypted.write(line+"\n")
            file.close()
            file_decrypted.close()
    
if __name__ == '__main__':
    #call script itself to decrypt encrypted log files
    ll = encryptlog.EncryptedLogFormatter()
    key = ll.kkmh #this is the way to get it back if you lost the encryption key
    #encryption key variable name supposed to be obfuscated to prevent prediction, there is no private/protected variable concept in python
    logFile_decrypt(key)