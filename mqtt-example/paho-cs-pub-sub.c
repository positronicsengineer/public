/*******************************************************************************
 * Copyright (c) 2012, 2013 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *   http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Ian Craggs - initial contribution
 *    Ian Craggs - change delimiter option from char to string
 *    Guilherme Maciel Ferreira - add keep alive option
 *
 * Change notes by positronics.engineer at yandex dot com.
 *    positronics.engineer - reference ../paho.mqtt.c/src/samples/paho_cs_sub.c - from master tag on 16022018
 *    positronics.engineer - publish and subscribe with the same client, no such an example in lib - 07/2018
 *    positronics.engineer - add a timer to publish periodically - 08/2018
 *    positronics.engineer - revise for sharing at positronicsengineer.wordpress.com - 10/2019
 *******************************************************************************/

/*
 subscriber/publisher example
    Topics are hard coded in source code

 parameters (see opts_struct for default assignments):
 
	--host
	--port
	--qos
	--delimiter
	--clientid
	--showtopics
	--keepalive
	--userid
	--password

*/
#include "MQTTClient.h"
#include "MQTTClientPersistence.h"

#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#define INTERVAL 2*60	//seconds, for periodic publish

const char *roottopic = "mqttexample";
const char *datatopic = "senddata";
const char *reqtopic = "requestdata";
//topics will be "roottopic/clientid/topic"
//it should subscribe roottopic/+/topic

//message
const char *one = "1";
const char *zero = "0";
const char *msgbdy = "Data req #";
int reqcount = 0;

volatile int toStop = 0;
volatile int toPub = 0;
volatile int connectlost = 0;
volatile MQTTClient_deliveryToken deliveredtoken;

char* concat_topic(const char *topic1, const char *topic2, const char *topic3, const char *topic4);

MQTTClient client;

struct opts_struct
{
	char* clientid;
	int nodelimiter;
	char* delimiter;
	int qos;
	char* username;
	char* password;
	char* host;
	char* port;
	int showtopics;
	int keepalive;
	int maxdatalen;
	int retained;
	int verbose;
} opts =
{
	"paho-mqtt", 0, "\n", 0, NULL, NULL, "broker.hivemq.com", "1883", 0, 90, 100, 0, 0
}; //defaults

void delivered(void *context, MQTTClient_deliveryToken dt)
{
    printf("Message with token value %d delivery confirmed\n", dt);
    deliveredtoken = dt;
}

//publish to defined topic
void senddata(void)
{
    int data_len=0;
    char *l_topic=NULL, buffer[100];
    memset(buffer, 0, sizeof(buffer)); //clear buffer for usage
    
    sprintf(buffer, "%s %d\n", msgbdy, reqcount);
    reqcount++;

    data_len = (int)strlen(buffer);
    l_topic = concat_topic(roottopic, opts.clientid, datatopic, NULL);
    MQTTClient_publish(client, l_topic, data_len, buffer, opts.qos, opts.retained, NULL);
    printf("Publishing to topic : %s\n", l_topic);
    printf("Published message : %s", buffer);
        
    free(l_topic);
}

//check arrived messages, if relevant take actions
int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
    int i;
    char* payloadptr; 
    char match=0, buffstr[100]="/"; //define a string starting with "/", assuming any topic string shorter than 100 chars, memory must be reserved since later will use for strcat()

    printf("A new message arrived to topic: %s\n", topicName);
    printf("Message:");

    payloadptr = message->payload;
    for(i=0; i < message->payloadlen; i++)
    {
        putchar(*(payloadptr+i));
    }
    putchar('\n');

    //printf("Checking topic for proper action\n");
    //sometimes strcmp() returns wrong value, may be message->payload has no NULL at the end,
    //so compare first message->payloadlen number of chars, strncmp()
    if (strstr(topicName, strcat(buffstr, reqtopic))!= NULL)
    {
    	if (strncmp(payloadptr, "1", message->payloadlen)== 0) //check message
    	{
            printf("Responsing to valid message\n");
            senddata();
    	}
    	else
    	{	
            printf("No response to invalid message\n");
            printf("Invalid message to topic: %s\n", topicName);
            printf("Invalid message: %.*s\n", message->payloadlen, payloadptr); //it seems that there is no NULL at the end of message->payload
        }
        match = 1;
    }
    
    //my suggested method to check any "/othertopic/subtopic"
    /* strcpy(buffstr,"/"); //init buffer again to /
    if (strstr(topicName, strcat(strcat(strcat(buffstr, "othertopic"), "/"),"subtopics"))!= NULL) // if contains "/othertopic/subtopics"
    {
        //check message
        //do task
        match = 1;
    } */
    
    if (match == 0)
    	printf("No action defined for topic: %s\n", topicName);

    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}


void connlost(void *context, char *cause)
{
    printf("\nConnection lost\n");
    printf("     cause: %s\n", cause);
	connectlost = 1;
}


void usage(void)
{
	printf("MQTT example, client subscribes and publishes to defined topics in source code.\n");
	printf("Usage: <exe-name> <options>, where options are:\n");
	printf("  --host <hostname> (default is %s)\n", opts.host);
	printf("  --port <port> (default is %s)\n", opts.port);
	printf("  --qos <qos> (default is %d)\n", opts.qos);
	printf("  --delimiter <delim> (default is \\n)\n");
	printf("  --clientid <clientid> (default is %s), 4 random numbers will be added to it\n", opts.clientid);
	printf("  --username none, for private -non public- brokers\n");
	printf("  --password none, for private -non public- brokers\n");
	printf("  --showtopics <on or off> (default is on if the topic has a wildcard, else off)\n");
	printf("  --keepalive <seconds> (default is %d seconds)\n", opts.keepalive);
	exit(EXIT_FAILURE);
}


void myconnect(MQTTClient* client, MQTTClient_connectOptions* opts)
{
	int rc = 0;
	printf("Connecting\n");
	if ((rc = MQTTClient_connect(*client, opts)) != 0)
	{
		printf("Failed to connect, return code %d\n", rc);
		exit(EXIT_FAILURE);
	}
	else 
		printf("Connected successfully!\n");
}


void cfinish(int sig)
{
	signal(SIGINT, NULL);
	toStop = 1;
}

void alarm_signal (int sig)
{
	toPub = 1;
}

char* itoa (int value)
{
    int length = snprintf( NULL, 0, "%d", value );
    char* str = (char *)malloc( length + 1 );
    snprintf( str, length + 1, "%d", value );
    return str;
    //free(str);
}

void getopts(int argc, char** argv);

int main(int argc, char** argv)
{
	//MQTTClient client;
	MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
	MQTTClient_SSLOptions ssl_opts = MQTTClient_SSLOptions_initializer;
	char* sub_topic = NULL;  //this is subscribe topic
	char* pub_topic = NULL; //publish topic
	char* buffer = NULL; //message buffer ptr
	int rc = 0, data_len = 0, delim_len = 0;
	char url[100];
	struct itimerval tout_val; //set a timer alarm for periodic publish
	
	tout_val.it_value.tv_sec = INTERVAL; //the time when timer expires and fires alarm signal
	tout_val.it_value.tv_usec = 0;
	tout_val.it_interval.tv_sec = INTERVAL; //reload this value to timer after it expires
	tout_val.it_interval.tv_usec = 0;
	//call signal() before setitimer()
    
	getopts(argc, argv);
	sprintf(url, "%s:%s", opts.host, opts.port);
    printf("Going to be connect %s\n", url);

    //define random clientid
    char clientid[25] = "\0"; //assign null char to array, final clientid must be less than defined char array
    int id = time(NULL);
    srand(id);
    id = rand()%10000; //limit digit number to 4
    strcat(clientid, opts.clientid);
    strcat(clientid, itoa(id));
	opts.clientid = clientid;
    printf("Clientid is set as %s\n",opts.clientid);
    
    delim_len = (int)strlen(opts.delimiter);
	buffer = malloc(opts.maxdatalen);
	rc = MQTTClient_create(&client, url, opts.clientid, MQTTCLIENT_PERSISTENCE_NONE, NULL);
    if (rc != MQTTCLIENT_SUCCESS)
    {
		printf("Couldn't create client, exit!!!\n");
        exit(EXIT_FAILURE);
    }
	signal(SIGINT, cfinish);
	signal(SIGTERM, cfinish);
	signal(SIGALRM, alarm_signal); /* set the Alarm signal capture */
	
    conn_opts.keepAliveInterval = opts.keepalive;
	conn_opts.reliable = 0;
	conn_opts.cleansession = 1;
	conn_opts.username = opts.username;
	conn_opts.password = opts.password;
	ssl_opts.enableServerCertAuth = 0;
	conn_opts.ssl = &ssl_opts;
	buffer = malloc(opts.maxdatalen);
	
	MQTTClient_setCallbacks(client, NULL, connlost, msgarrvd, delivered);
	myconnect(&client, &conn_opts);
    
    //Subscribe related topics
    sub_topic = concat_topic(roottopic, "+", reqtopic, NULL);
    printf("Subscribing to topic: %s\n",sub_topic);
	rc = MQTTClient_subscribe(client, sub_topic, opts.qos);
	//if successful return MQTTCLIENT_SUCCESS which is defined as 0
	if (rc != MQTTCLIENT_SUCCESS)
		printf("Couldn't subscribe to topic %s\n",sub_topic);
    free(sub_topic); //free() since concat_topic malloc()

	setitimer(ITIMER_REAL, &tout_val,0); //set timer for periodic publish
    
	while (!toStop)
	{
		//if connection lost reconnect
		if (connectlost == 1)
		{
			connectlost = 0;
			printf("Reconnecting since connection lost!\n");
			myconnect(&client, &conn_opts);
            
            //subscribe again when connection lost
            sub_topic = concat_topic(roottopic, "+", reqtopic, NULL);
            printf("Subscribing to topic: %s after connection lost!\n",sub_topic);
            rc = MQTTClient_subscribe(client, sub_topic, opts.qos);
            //if (rc != MQTTCLIENT_SUCCESS)
            //    printf("Couldn't subscribe to topic %s\n",topic);
            free(sub_topic);
		}
		
		if (toPub == 1)
		{
			toPub = 0;
            printf("Periodic publishing\n");
			
            senddata();
		}
		sleep(10);
	}

	printf("Stopping!!!\n");
	free(buffer);
    //free(sub_topic);
    //free(pub_topic);
	//MQTTClient_unsubscribe(client, sub_topic);
	MQTTClient_disconnect(client, 1000);
	MQTTClient_destroy(&client);
	return EXIT_SUCCESS;
}

//parse command line arguments
void getopts(int argc, char** argv)
{
	int count = 1;
	while (count < argc)
	{
		if (strcmp(argv[count], "--qos") == 0)
		{
			if (++count < argc)
			{
				if (strcmp(argv[count], "0") == 0)
					opts.qos = 0;
				else if (strcmp(argv[count], "1") == 0)
					opts.qos = 1;
				else if (strcmp(argv[count], "2") == 0)
					opts.qos = 2;
				else
					usage();
			}
			else
				usage();
		}
		else if (strcmp(argv[count], "--host") == 0)
		{
			if (++count < argc)
				opts.host = argv[count];
			else
				usage();
		}
		else if (strcmp(argv[count], "--port") == 0)
		{
			if (++count < argc)
				opts.port = argv[count];
			else
				usage();
		}
		else if (strcmp(argv[count], "--clientid") == 0)
		{
			if (++count < argc)
				opts.clientid = argv[count];
			else
				usage();
		}
		else if (strcmp(argv[count], "--username") == 0)
		{
			if (++count < argc)
				opts.username = argv[count];
			else
				usage();
		}
		else if (strcmp(argv[count], "--password") == 0)
		{
			if (++count < argc)
				opts.password = argv[count];
			else
				usage();
		}
		else if (strcmp(argv[count], "--delimiter") == 0)
		{
			if (++count < argc)
				opts.delimiter = argv[count];
			else
				opts.nodelimiter = 1;
		}
		else if (strcmp(argv[count], "--showtopics") == 0)
		{
			if (++count < argc)
			{
				if (strcmp(argv[count], "on") == 0)
					opts.showtopics = 1;
				else if (strcmp(argv[count], "off") == 0)
					opts.showtopics = 0;
				else
					usage();
			}
			else
				usage();
		}
		else if (strcmp(argv[count], "--keepalive") == 0)
		{
			if (++count < argc)
				opts.keepalive = atoi(argv[count]);
			else
				usage();
		}
        else if ((strcmp(argv[count], "--help") == 0)|(strcmp(argv[count], "-h") == 0))
        {
        	usage();
        }
        else
        {
            printf("Invalid option!!!\n");
        	usage();
        }
		count++;
	}
}

/*concat 4 strings as topic,
  send NULL if no more concat is needed, send topic wild-chars as "+", "#"
  need to free() explicitly in caller */
char* concat_topic(const char *topic1, const char *topic2, const char *topic3, const char *topic4)
{
    int len = strlen(topic1)+strlen(topic2)+1; //+1 for "/" char

    if (!(topic3 == NULL))
    {
        len += strlen(topic3) + 1;  //one more "/" char
    }

    if (!(topic4 == NULL))
    {
        len += strlen(topic4) + 1;  //one more "/" char
    }

    char *topic = (char *)calloc ((len+1),sizeof(char)); //len+1 for null char at the end
    strcat(topic, topic1);
    strcat(topic, "/");
    strcat(topic, topic2);
    if (!(topic3 == NULL))
    {
        strcat(topic, "/");
        strcat(topic, topic3);
    }

    if (!(topic4 == NULL))
    {
        strcat(topic, "/");
        strcat(topic, topic4);
    }
    return topic;
}
