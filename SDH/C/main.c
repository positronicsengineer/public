//Copyright (c) 2016 positronics.engineer at yandex dot com.
//Distributed under the terms of the MIT license.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
/*OPENCV headers*/
#include "opencv2\imgproc\imgproc_c.h"
#include "opencv2\highgui\highgui_c.h"
#include "opencv2\core\core_c.h"
/*MATLAB headers*/
#include "mat.h"
/*My headers*/
#include "myimproc.h"
#include "mymatlab.h"
#include "myextern_array.h"

#define NG 256

int main( int argc, char** argv )
{
    int height, width, step, channels, depth, nsize, i, j;
    char *in_image, *mat_file, *mat_var, *text_file, *diff_mat_file, *diff_mat_var;
    struct SDH *my_sdh=NULL;
    clock_t start_t, end_t;

    /* data structure for the image */
    IplImage *img=0;

    FILE *outf=NULL;    //file output

    start_t = clock();  //measure run-time
    if( argc < 2 )
    {
        fprintf( stderr, "Usage: *.exe <in_image> <sum_hist_mat_file> <sum_mat_variable> <diff_hist_mat_file> <diff_mat_variable> <text_file>\n" );
        return 1;
    }
    in_image = argv[1];
    mat_file = argv[2];
    mat_var = argv[3];
    diff_mat_file = argv[4];
    diff_mat_var = argv[5];
    text_file = argv[6];

    //load the image, use CV_LOAD_IMAGE_UNCHANGED for original
    img = cvLoadImage(in_image, CV_LOAD_IMAGE_GRAYSCALE);

    if( img == 0 )
    {
        fprintf( stderr, "Cannot load file %s!\n", in_image );
        return 1;
    }

    height = img->height;
    width = img->width;
    depth = img->depth;
    step = img->widthStep;
    channels = img->nChannels;
	nsize = img->nSize;

    printf("\n%s Image Properties (imported as gray scale)\n",in_image);
    printf("----------------------------\n");
    printf("image nSize is %d\n", nsize);
    printf("image depth is %d\n", depth);
    printf("image height is %d\n", height);
    printf("image width is %d\n", width);
    printf("image step is %d\n", step);
    printf("image channel is %d\n", channels);


    my_sdh=my_SumDiffHist(img,NG);

	//saving as MAT files
	saveasMAT2(1,2*NG-1,my_sdh->sum,mat_file,mat_var);
	printf("----------------------------\n");
    printf("Saving %s\n", mat_file);
    printf("Variable name is %s\n", mat_var);

    saveasMAT2(1,2*NG-1,my_sdh->diff,diff_mat_file,diff_mat_var);
    printf("----------------------------\n");
    printf("Saving %s\n", diff_mat_file);
    printf("Variable name is %s\n", diff_mat_var);

    end_t = clock();
    printf("----------------------------\n");
    printf("Started at %ld, finished at %ld, it takes %ld ticks, elapsed time is %f seconds\n", start_t, end_t, end_t-start_t, (end_t - start_t)/(double)CLOCKS_PER_SEC);

    //writing data into a text file
    if((outf=fopen(text_file,"w"))==NULL)
    {
        fprintf(stderr, "%s file cannot be opened\n", text_file);
        return 1;
    }

    //output sum and diff hist to txt file
    printf("sum and diff histogram values printed to %s \n(SUM\t DIFF)\n",text_file);
    for(i=0;i<2*NG-1;i++)
    {
        fprintf(outf, "%f\t %f\n",*(my_sdh->sum+i),*(my_sdh->diff+i));
        //fprintf(outf, "\n");
    }

    //uncomment to print imported image pixel values to text file
    /*
    printf("printing image pixel values into %s\n",text_file);
    for(i=0;i<height;i++)
		 {
		     for(j=0;j<width;j++)
			 {
			     fprintf(outf, "%d\t",MyGetPixel(img,i,j,0));
			 }
			 fprintf(outf, "\n");
		 }
    */
    //END printing image pixels to text file

    fclose(outf);
    printf("----------------------------\n");
    printf("%s file has been created\n",text_file);
    //end writing text file


    //release image
    cvReleaseImage( &img );
    free(my_sdh->sum);
    free(my_sdh->diff);
    free(my_sdh);

return 0;   /*END*/
}


/*
Examples
    const char *output_RGB="Modified_RGB.bmp";
    const char *output_GRY="Modified_GRY.bmp";
    char value=0;
    int params[3] = {1,10,0};

    //image pixel get value
    printf("image pixel 3,3 B value is %d\n", MyGetPixel(img,3,3,B));
    printf("image pixel 3,3 B value is %d\n", MyGetPixel2(img,3,3,B));

    //image pixel set value
    if(!MySetPixel(img,3,3,R,value))
        printf("Setting image pixel value cannot succeeded!\n");

   //Saving modified image
    if(!cvSaveImage(output_RGB,img,params))
        printf("Could not save: %s\n",output_RGB);

    //Convert to gray image
    img2=cvCreateImage(cvSize(width,height),IPL_DEPTH_8U,1);
	cvCvtColor(img, img2, CV_RGB2GRAY);     //change image color space, here convert to Grey

	//simple method to measure run time performance
    start_t = clock();
    end_t = clock();
    printf("Started at %ld, finished at %ld, elapsed %f ms\n", start_t, end_t, (end_t - start_t)/(double)CLOCKS_PER_SEC);

*/
