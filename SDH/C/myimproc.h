//Copyright (c) 2016 positronics.engineer at yandex dot com.
//Distributed under the terms of the MIT license.

#ifndef MY___IMPROCH
#define MY___IMPROCH

#define B 0     //or gray image, single channel image
#define G 1
#define R 2
//color component/channel parameter can be called as B,R,G symbols

struct SDH
{
    double *sum;
    double *diff;
};


unsigned int MyGetPixel (IplImage* img, unsigned int row, unsigned int col, unsigned int color_ch);
unsigned int MyGetPixel2 (IplImage* img, unsigned int row, unsigned int col, unsigned int color_ch);
unsigned int MySetPixel (IplImage* img, unsigned int row, unsigned int col, unsigned int color_ch, unsigned int value);
struct SDH *my_SumDiffHist (IplImage *img, int Ng);

#endif //myimproc.h
