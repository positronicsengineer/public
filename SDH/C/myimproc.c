//Copyright (c) 2016 positronics.engineer at yandex dot com.
//Distributed under the terms of the MIT license.

#include <stdio.h>
#include "opencv2\core\core_c.h"
#include "myimproc.h"

/**************************************************************/
//rows -> height
//cols -> width
//returns desired pixel value
//Pixel indexes start from zero
//Initial pixel p(0,0)
unsigned int MyGetPixel (IplImage* img, unsigned int row, unsigned int col, unsigned int color_ch)
{
	unsigned int height, width, row_width, channel_num, depth, index, value;

	height = img->height;
    width = img->width;
    depth = img->depth;
    row_width = img->widthStep;
    channel_num = img->nChannels;

	if (col > width || row > height)
		{
			printf("MyGetPixel::Pixel index is greater than image!\n");
			exit(-1);
		}

	//if (depth!=8 || channel_num!=3)
	if (depth!=8)
		{
			printf("MyGetPixel::Only 8bit images supported!\n");
			exit(-1);
		}

	index = row*row_width + col*channel_num + color_ch;	//calculate pointer offset
	value = (unsigned int)((unsigned char)(*(img->imageData+index)));	//direct type casting from char to uint is not working as expected!
	return value;
}
/**************************************************************/

/**************************************************************/
//same as MyGetPixel but uses OPenCV built-in cvGet2D function
unsigned int MyGetPixel2 (IplImage* img, unsigned int row, unsigned int col, unsigned int color_ch)
{
    CvScalar s;
    unsigned int pixel;
    s=cvGet2D(img,row,col);
    pixel=s.val[color_ch];
    return pixel;
}
/**************************************************************/

/**************************************************************/
// sets desired pixel value
unsigned int MySetPixel (IplImage* img, unsigned int row, unsigned int col, unsigned int color_ch, unsigned int value)
{
	unsigned int height, width, row_width, channel_num, depth, index;

	height = img->height;
    width = img->width;
    depth = img->depth;
    row_width = img->widthStep;
    channel_num = img->nChannels;

	if (col > width || row > height)
		{
			printf("MySetPixel::Pixel index is greater than image!\n");
			exit(-1);
		}

	//if (depth!=8 || channel_num!=3)
	if (depth!=8)
		{
			printf("MySetPixel::Only 8bit images supported!\n");
			exit(-1);
		}

	index = row*row_width + col*channel_num + color_ch;	//calculate pointer offset
	(*(img->imageData+index)) = value;
	return 1;
}
/**************************************************************/

/**************************************************************/
/*this function calculates the sum and difference histogram of the given image array(matrix)
**Ng is the number of gray levels in the image
**img pointer is the starting point of the image (returned by OpenCV lib)
**rows and cols are dimensions of the image array
**-----------
**-+++++++-
**-+++++++-
**-+++++++-
**-+++++++-
**-----------
**+ signed pixels of the image are used to obtain histogram
**eight neighborhood is use
**+++
**+o+
**+++
**+ signed neighboring pixels of "o" are used
**if data between [0,Ng-1] (Ng level) then sum histogram is between [0,2*Ng-2] (2*Ng-1 level)
**if data between [0,Ng-1] (Ng level) then diff histogram is between [-(Ng-1),Ng-1] (2*Ng-1 level)
**it allocates necessary memory and returns a SDH data structure pointer
**SDH data structure defined in header file
*/
struct SDH *my_SumDiffHist (IplImage *img, int Ng)
{
	unsigned int height, width, row_width, channel_num, depth, i, j;
	struct SDH *sdh;

	height = img->height;
    width = img->width;
    depth = img->depth;
    row_width = img->widthStep;
    channel_num = img->nChannels;

    sdh = (struct SDH *)calloc(1,sizeof (struct SDH *)); /* Allocating memory for SDH struct */
    sdh->sum = (double *)calloc(2*Ng-1, sizeof (double)); /* Allocating data for sum histogram*/
    sdh->diff = (double *)calloc(2*Ng-1, sizeof (double)); /* Allocating data for diff histogram*/

	 if(sdh==NULL||sdh->sum==NULL||sdh->diff==NULL)
		{printf("my_SumDiffHist::Memory cannot be allocated, NULL data pointer\n");
		  exit(-1);
		}

	if (channel_num > 1)
		{
			printf("my_SumDiffHist::Only single channel/gray images supported!\n");
			exit(-1);
		}

	for(i=1;i<=(height-2);i++)
		 for(j=1;j<=(width-2);j++)
			 {
			  (*(sdh->sum+(MyGetPixel(img,i,j,0)+MyGetPixel(img,i-1,j-1,0))))++;
			  (*(sdh->sum+(MyGetPixel(img,i,j,0)+MyGetPixel(img,i-1,j,0))))++;
			  (*(sdh->sum+(MyGetPixel(img,i,j,0)+MyGetPixel(img,i-1,j+1,0))))++;
			  (*(sdh->sum+(MyGetPixel(img,i,j,0)+MyGetPixel(img,i,j-1,0))))++;
			  (*(sdh->sum+(MyGetPixel(img,i,j,0)+MyGetPixel(img,i,j+1,0))))++;
			  (*(sdh->sum+(MyGetPixel(img,i,j,0)+MyGetPixel(img,i+1,j-1,0))))++;
			  (*(sdh->sum+(MyGetPixel(img,i,j,0)+MyGetPixel(img,i+1,j,0))))++;
			  (*(sdh->sum+(MyGetPixel(img,i,j,0)+MyGetPixel(img,i+1,j+1,0))))++;
			  //once looping image, create diff histogram, too, can be separate function but bad for run time performance
			  (*(sdh->diff+((int)MyGetPixel(img,i,j,0)-(int)MyGetPixel(img,i-1,j-1,0)+Ng-1)))++;
			  (*(sdh->diff+((int)MyGetPixel(img,i,j,0)-(int)MyGetPixel(img,i-1,j,0)+Ng-1)))++;
			  (*(sdh->diff+((int)MyGetPixel(img,i,j,0)-(int)MyGetPixel(img,i-1,j+1,0)+Ng-1)))++;
			  (*(sdh->diff+((int)MyGetPixel(img,i,j,0)-(int)MyGetPixel(img,i,j-1,0)+Ng-1)))++;
			  (*(sdh->diff+((int)MyGetPixel(img,i,j,0)-(int)MyGetPixel(img,i,j+1,0)+Ng-1)))++;
			  (*(sdh->diff+((int)MyGetPixel(img,i,j,0)-(int)MyGetPixel(img,i+1,j-1,0)+Ng-1)))++;
			  (*(sdh->diff+((int)MyGetPixel(img,i,j,0)-(int)MyGetPixel(img,i+1,j,0)+Ng-1)))++;
			  (*(sdh->diff+((int)MyGetPixel(img,i,j,0)-(int)MyGetPixel(img,i+1,j+1,0)+Ng-1)))++;
              /*typecast uint to int and add Ng-1 to each index to prevent negative indexing
                remember that diff histogram may contains negative elements (minimum is -Ng+1)*/
			 }
	/*increase the histogram array element by one
	clarification with parenthesis; (*(ptr+offset))++
	first add offset to ptr then direct the value inside and increase it by one
	*(ptr+offset)++ generates error because of operator priority*/
	 return sdh;
}
/**************************************************************/
