//Copyright (c) 2016 positronics.engineer at yandex dot com.
//Distributed under the terms of the MIT license.

#ifndef MY___EXTARRAYH
#define MY___EXTARRAYH

double** import2Darray(const char *infile, double** array, int *row_num, int *col_num);
double** allocate2D(double** A, const int row, const int col);
void free2Darray(double** p);

#endif //myextern_array.h
