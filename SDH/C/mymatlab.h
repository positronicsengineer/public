//Copyright (c) 2016 positronics.engineer at yandex dot com.
//Distributed under the terms of the MIT license.

#ifndef MY___MATLABH
#define MY___MATLABH

void saveasMAT2(int rows,int cols,double *input,char *output_name,char *var_name);
double *importMAT(char *file, int *rows_addr,int *cols_addr);
int convert2dim_indexes(int i,int j,int rows,int cols);
double getdata(double *dataptr,int i,int j,int rows,int cols);

#endif //mymatlab.h
