function [sum_hist,diff_hist]=my_sdh(I,Ng)
%computes the sum and difference histograms
%of image I, returns sum and difference histograms
%[sum_hist,dif_hist]=my_sdh(I,Ng)
%where I is the image data and Ng is the number of gray levels
%image pixels are in the interval [0,Ng-1]
%
%to compute sdh the + signed neighbour pixels which are 
%in 8 neighbourhood of the * pixel, are used 
%   + + +
%   + * +
%   + + +
%
%some of the image pixels are out of computation because of the edges
%   . . . . . . . . 
%   . * * * * * * .
%   . * * * * * * .
%   . . . . . . . .
%sdh are computed only for * signed pixel on such a 4x8 image I

%Copyright (c) 2016 positronics.engineer at yandex dot com.
%Distributed under the terms of the MIT license.

[m,n]=size(I);
I=double(I);    %make image double, if it is uint8, calculates wrong results

sum_hist(1:2*Ng-1)=0;
diff_hist(1:2*Ng-1)=0;
for i=2:m-1
    for j=2:n-1
        sum_hist(1,I(i,j)+I(i-1,j-1)+1)=sum_hist(1,I(i,j)+I(i-1,j-1)+1)+1;
        sum_hist(1,I(i,j)+I(i-1,j)+1)  =sum_hist(1,I(i,j)+I(i-1,j)+1)+1;
        sum_hist(1,I(i,j)+I(i-1,j+1)+1)=sum_hist(1,I(i,j)+I(i-1,j+1)+1)+1;
        sum_hist(1,I(i,j)+I(i,j+1)+1)  =sum_hist(1,I(i,j)+I(i,j+1)+1)+1;
        sum_hist(1,I(i,j)+I(i,j-1)+1)  =sum_hist(1,I(i,j)+I(i,j-1)+1)+1;
        sum_hist(1,I(i,j)+I(i+1,j+1)+1)=sum_hist(1,I(i,j)+I(i+1,j+1)+1)+1;
        sum_hist(1,I(i,j)+I(i+1,j)+1)  =sum_hist(1,I(i,j)+I(i+1,j)+1)+1;
        sum_hist(1,I(i,j)+I(i+1,j-1)+1)=sum_hist(1,I(i,j)+I(i+1,j-1)+1)+1;
        
        diff_hist(1,I(i,j)-I(i-1,j-1)+1+Ng-1)=diff_hist(1,I(i,j)-I(i-1,j-1)+1+Ng-1)+1;
        diff_hist(1,I(i,j)-I(i-1,j)+1+Ng-1)  =diff_hist(1,I(i,j)-I(i-1,j)+1+Ng-1)+1;
        diff_hist(1,I(i,j)-I(i-1,j+1)+1+Ng-1)=diff_hist(1,I(i,j)-I(i-1,j+1)+1+Ng-1)+1;
        diff_hist(1,I(i,j)-I(i,j+1)+1+Ng-1)  =diff_hist(1,I(i,j)-I(i,j+1)+1+Ng-1)+1;
        diff_hist(1,I(i,j)-I(i,j-1)+1+Ng-1)  =diff_hist(1,I(i,j)-I(i,j-1)+1+Ng-1)+1;
        diff_hist(1,I(i,j)-I(i+1,j+1)+1+Ng-1)=diff_hist(1,I(i,j)-I(i+1,j+1)+1+Ng-1)+1;
        diff_hist(1,I(i,j)-I(i+1,j)+1+Ng-1)  =diff_hist(1,I(i,j)-I(i+1,j)+1+Ng-1)+1;
        diff_hist(1,I(i,j)-I(i+1,j-1)+1+Ng-1)=diff_hist(1,I(i,j)-I(i+1,j-1)+1+Ng-1)+1;    
    end
end
