%Copyright (c) 2016 positronics.engineer at yandex dot com.
%Distributed under the terms of the MIT license.

%necessary time to extract sum and difference histograms (SDH) for one image sample
clear
clc

disp ('Calculation time with MATLAB')
tic
im=imread('Sample1600x1200.bmp','bmp');
im_gray=rgb2gray(im);

[MATsum,MATdiff]=my_sdh(im_gray,256);
save('MATsum.mat','MATsum');
save('MATdiff.mat','MATdiff');
toc

disp ('Calculation time with C executable')
tic
[status, stdio] = system('sdh_calculate.exe Sample1600x1200.bmp Csum.mat Csum CDiff.mat Cdiff out.txt');
toc

load('Csum.mat');
load('Cdiff.mat')

if(isequal(Csum',MATsum))
    disp ('sum histograms are equal')
else
    disp ('problem on sum histograms')
end

if(isequal(Cdiff',MATdiff))
    disp ('diff histograms are equal')
else
    disp ('problem on diff histograms')
end