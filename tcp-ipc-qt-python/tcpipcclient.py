#!/usr/bin/python3

#Copyright (c) 2020 positronics.engineer at yandex dot com.
#Distributed under the terms of the MIT license.

#basic tcp based ipc client, to send and receive interprocess messages over localhost
#initial release, Jan 2020

import socket
import logging

tcpserver_address = ('localhost', 9999)
tcpsrv_warn_msg = "No TCP server available to connect!"
sock_timeout = 1 #sec
Ntry = 3 #number of tries to receive ACK msg
ACKmsg = "__ACK"
NoACKmsg = "__NoACK"

class TcpIpcClient(object):

    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if not(self.sock.connect_ex(tcpserver_address)):
            self.tcpsrvAlive = 1 #there is a tcp server available to connect
            logging.debug("Connected to %s port %s" % tcpserver_address)
        else:
            self.tcpsrvAlive = 0 #if there is no tcp server available, set it to zero
            logging.debug(tcpsrv_warn_msg)
            
    def ClntSendMsg(self, msg):
        if(self.tcpsrvAlive):
            logging.debug("Sending message: %s", msg)
            if not (isinstance(msg, bytes)):
                logging.debug("Msg is not bytearray, encoded into ascii while sending")
                self.sock.sendall(msg.encode("ascii"))
            else:
                self.sock.sendall(msg)
        else:
            logging.debug(tcpsrv_warn_msg)

    def ClntRecvMsg(self):
        if(self.tcpsrvAlive):
            msg = self.sock.recv(1024)
            msg = msg.decode("ascii")
            return msg
        else:
            logging.debug(tcpsrv_warn_msg)
            return ""
    
    #try N times to read ACKmsg from buffer, if not available at first read, 
    #then time out in predefined period sock_timeout
    #all recv() are non-blocking in this function
    #if no ACKmsg is received at all, it appends a NoACKmsg to received message and returns
    def ClntRecvACK(self):
        if(self.tcpsrvAlive):
            self.sock.settimeout(5*sock_timeout)
            msg = self.sock.recv(1024)
            msg = msg.decode("ascii")
            logging.debug('Message received: %s', msg)
            self.sock.settimeout(None)
            
            if not ACKmsg in msg:
                logging.debug('No %s in message, will try %d times to get it' % (ACKmsg, Ntry))
                for i in range (1,Ntry+1):
                    try:
                        self.sock.settimeout(sock_timeout)
                        msg2 = self.sock.recv(1024)
                        msg2 = msg2.decode("ascii")
                        msg = msg + msg2
                        if ACKmsg in msg:
                            logging.debug('try #%d, %s received in: %s' % (i, ACKmsg, msg2))
                            self.sock.settimeout(None)
                            return msg
                        else:
                            logging.debug('try #%d, A new msg received: %s, not includes %s' % (i, msg2, ACKmsg))
                    except socket.timeout:
                        logging.debug("try #%d, Socket timed out, skip waiting %s" % (i, ACKmsg))
                logging.debug("Tried %d times, skip waiting and finish", i)
                return msg+NoACKmsg
            else:
                return msg
        else:
            logging.debug(tcpsrv_warn_msg)
            return "NoTCPServer"
    
    def ClntClose(self):
        if(self.tcpsrvAlive):
            self.sock.close()

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    print("Testing class and functions")
    tcpipc = TcpIpcClient()
    tcpipc.ClntSendMsg("Hello")
    #msg = tcpipc.ClntRecvMsg()
    msg = tcpipc.ClntRecvACK()
    print("Received message: %s" % msg)
    tcpipc.ClntClose()