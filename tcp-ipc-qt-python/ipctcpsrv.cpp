//Copyright (c) 2020 positronics.engineer at yandex dot com.
//Distributed under the terms of the MIT license.

#include "ipctcpsrv.h"
#include <QThread>

ipcTcpSrv::ipcTcpSrv(QObject *parent) : QObject(parent)
{

    server = new QTcpServer(this);

        // whenever a user connects, it will emit signal
        connect(this->server, SIGNAL(newConnection()), this, SLOT(newConnection()));

        if(!server->listen(QHostAddress::LocalHost, 9999))
        {
            qDebug() << "Server could not start";
            exit(1);
        }
        else
            qDebug()<<"Server started";
}

void ipcTcpSrv::sendMsg(QByteArray msg)
{
    socket->write(msg);
    socket->flush();
}

/* as example this function sends several messages to client with different timings
 * python client function ClntRecvACK() tries to read related ACK message as a response
 * you can change sleep times and sent messages to see how ClntRecvACK() behaves.
 */
void ipcTcpSrv::receiveMsg()
{
    qDebug()<<"Message is: "<<socket->readAll();

    QByteArray msg = "MSG__ACK";
    QThread::msleep(10);
    qDebug()<<"Send message: "<<msg;
    sendMsg(msg);

    msg = "ACK1";
    QThread::msleep(300);
    qDebug()<<"Send message: "<<msg;
    sendMsg(msg);

    msg = "ACK2";
    QThread::msleep(1500);
    qDebug()<<"Send message: "<<msg;
    sendMsg(msg);

    //socket->waitForBytesWritten(3000);
}

void ipcTcpSrv::closeSocket()
{
    socket->close();
    qDebug()<<"Socket closed, since client disconnected";
    qDebug()<<"----------------------------------------\n";
}

void ipcTcpSrv::newConnection()
{
    qDebug()<<"New connection received";
    // need to grab the socket
    socket = server->nextPendingConnection();
    connect(this->socket, SIGNAL(readyRead()), this, SLOT(receiveMsg()));
    connect(this->socket, SIGNAL(disconnected()), this, SLOT(closeSocket()));
}
