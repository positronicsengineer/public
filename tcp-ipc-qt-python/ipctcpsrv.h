//Copyright (c) 2020 positronics.engineer at yandex dot com.
//Distributed under the terms of the MIT license.

#ifndef IPCTCPSRV_H
#define IPCTCPSRV_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

class ipcTcpSrv : public QObject
{
    Q_OBJECT
public:
    explicit ipcTcpSrv(QObject *parent = 0);

signals:

public slots:

    void sendMsg(QByteArray msg);
    void receiveMsg();
    void newConnection();
    void closeSocket();

private:

    QTcpServer *server;
    QTcpSocket *socket;
};

#endif // IPCTCPSRV_H
