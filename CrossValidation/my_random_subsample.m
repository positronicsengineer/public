function [sampled_space,sampled_target]=my_random_subsample(all_data,all_target,k,l)
%[sampled_space,sampled_target]=my_random_subsample(all_data,all_target,k,l)
%returns a cell array which contains training and test samples
%are partition into 2 matrix with l sample is selected for testing, k times
%all_data contains the all sample space, 
%all_target contains the all target space, 
%k is the number of training sessions
%l is the number of samples randomly seperated for testing at each training section

%Copyright (c) 2016 positronics.engineer at yandex dot com.
%Distributed under the terms of the MIT license.

[m,n]=size(all_data);
for i=1:k
    index=randperm(n);%randomly select without replacement
    %create indexes
    train_index=sort(index([1:n-l]));
    test_index=sort(index([n-l+1:n]));
    %create data spaces
    sampled_space{i,1}=all_data(:,train_index);
    sampled_space{i,2}=all_data(:,test_index);
    %create targets
    sampled_target{i,1}=all_target(:,train_index);
    sampled_target{i,2}=all_target(:,test_index);
end