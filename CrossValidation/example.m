%Copyright (c) 2016 positronics.engineer at yandex dot com.
%Distributed under the terms of the MIT license.

%create your NN
%import your sample space and preprocess if necessary
k=10;%define number of folds, ie. 10
[folded_data,folded_target]=my_k_fold(dataAll,targetsAll,k);
for fold_loop=1:k
	train_data=folded_data{fold_loop,1};
	test_data=folded_data{fold_loop,2};
 
	train_targets=folded_target{fold_loop,1};
	test_targets=folded_target{fold_loop,2};
 
	%train and test your neural network in loop
 
end