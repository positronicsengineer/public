function [sampled_space,sampled_target]=my_bootstrap(all_data,all_target,k,j)
%[sampled_space,sampled_target]=my_bootstrap(all_data,all_target,k,j)
%returns a cell array which contains training and test samples
%are partition into 2 matrix with j sample is selected randomly for training with replacement
%all_data contains the all sample space, 
%all_target contains the all target space, 
%k is the number of training sessions
%j is the number of samples randomly selected for testing at each training section
%random selection is with replacement means any randomly selected sample might be selected again and again

%Copyright (c) 2016 positronics.engineer at yandex dot com.
%Distributed under the terms of the MIT license.

[m,n]=size(all_data);

for i=1:k
    h=1;
    test_index=[];
    train_index=sort(my_random_range(1,j,1,n));
    %select train samples with replacement, any training sample may selected again and again, it is random
    
    %remove the selected training samples form the all sample space, 
    %hence the rest is test space
    for l=1:n
        if sum((train_index-l)==0)==0
            test_index(1,h)=l;
            h=h+1;
        else

        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %i.e. train_index=[3 5 5 6];
    %train_index-1=[2 4 4 5];
    %[2 4 4 5]==0 generates [0 0 0 0]
    %sum([0 0 0 0])=0, then element 1 is not in the training space, put it in test space
    %similarly 
    %train_index-5=[-2 0 0 1];
    %[-2 0 0 1]==0 results [0 1 1 0]
    %sum [0 1 1 0]=2, then element 5 is in the training sample, do not put in test space
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %create train and test data spaces
    sampled_space{i,1}=all_data(:,train_index);
    sampled_space{i,2}=all_data(:,test_index);
    %create targets
    sampled_target{i,1}=all_target(:,train_index);
    sampled_target{i,2}=all_target(:,test_index);
end