function random_sequence=my_random_range(m,n,min,max)
%random_sequence=my_random_range(m,n,min,max)
%generate random array with size m by n
%between the range [min max]

%Copyright (c) 2016 positronics.engineer at yandex dot com.
%Distributed under the terms of the MIT license.

random_sequence=round((max-min)*rand(m,n)+min);