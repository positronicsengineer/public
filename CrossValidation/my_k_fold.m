function [folded_data,folded_target]=my_k_fold(all_data,all_target,k)
%[folded_data,folded_target]=my_k_fold(all_data,all_target,k)
%returns a cell array which contains training and test samples
%are partition into 2 matrix (training & test) and K folds
%all_data contains the all sample space, 
%all_target contains the all target space, 
%k is the number of folds
%if k equals to number of available samples, it is leave-one-out

%written 2007, publicly available 2016
%Copyright (c) 2016 positronics.engineer at yandex dot com.
%Distributed under the terms of the MIT license.

% folded data is stored in a cell array
% as given below
% _____________________
% |training   |test   |
% _____________________
% |training   |test   |
% _____________________
% |training   |test   |
% _____________________
% |training   |test   |
% _____________________


[m,n]=size(all_data);
fold_size=round(n/k);
folded_data=cell(k,2);
for i=1:k-1;
    %create indexes
    train_index=[[1:(i-1)*fold_size] [i*fold_size+1:n]];
    test_index=[(i-1)*fold_size+1:i*fold_size];
    %create data spaces
    folded_data{i,1}=all_data(:,train_index);
    folded_data{i,2}=all_data(:,test_index);
    %create targets
    folded_target{i,1}=all_target(:,train_index);
    folded_target{i,2}=all_target(:,test_index);
end

%last fold has a modified number of elements 
%if the number of folds is not
%a exact multiplier of the number of samples 
%i.e. 204 samples 10 folds

i=k;
train_index=[1:(i-1)*fold_size];
test_index=[(i-1)*fold_size+1:n];
folded_data{i,1}=all_data(:,train_index);
folded_data{i,2}=all_data(:,test_index);
folded_target{i,1}=all_target(:,train_index);
folded_target{i,2}=all_target(:,test_index);

%i.e. 10 fold for 200 samples
%each fold is equal and contains 20 sample
%i.e. 10 fold for 204 samples
%each fold contains round(204/10)=20 samples (except last fold)
%last fold contains 204-round(204/10)*(k-1) (which equals 9 here)24 elements
%i.e. 10 fold for 205 samples
%each fold contains round(205/10)=21 samples (except last fold)
%last fold contains 205-round(204/10)*(k-1) (which equals 9 here)16 elements