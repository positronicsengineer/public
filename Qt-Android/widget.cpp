#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    this->counter = 0;

    this->ui->textBrowser->setFontPointSize(75);
    this->ui->textBrowser->setText(QString::number(this->counter));
    this->ui->textBrowser->setAlignment(Qt::AlignCenter);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_inc_clicked()
{
    this->counter++;
    this->ui->textBrowser->setText(QString::number(this->counter));
    this->ui->textBrowser->setAlignment(Qt::AlignCenter);
}


void Widget::on_dec_clicked()
{
    this->counter--;
    this->ui->textBrowser->setText(QString::number(this->counter));
    this->ui->textBrowser->setAlignment(Qt::AlignCenter);
}
