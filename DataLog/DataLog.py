#!/usr/bin/env python

# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Change notes:
# Copyright (c) 2017 positronics.engineer at yandex dot com.
# Sources are modified and merged on purpose, June-2017
# Modified for DHT12 floating point output, March-2018

import subprocess
import sys
from datetime import datetime

#related to DHT12 sensor which is compatible to DHT11, single wire protocol
import Adafruit_DHT
SENSOR = Adafruit_DHT.DHT11
#connected to GPIO4.
PIN = 4

#related to spreadsheet
import json
import gspread
from oauth2client.service_account import ServiceAccountCredentials

# Oauth JSON File
GDOCS_OAUTH_JSON = '/home/pi/DataLogger.json'
#SCOPE
SCOPES = ['https://spreadsheets.google.com/feeds']
# Google Docs spreadsheet name.
GDOCS_SPREADSHEET_NAME = 'ConditionLog'
# Google Docs spreadsheet worksheet/tab name.
GDOCS_WORKSHEET_NAME = 'Data'

#DATE = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
DATE = datetime.now().strftime('%d-%m-%Y %H:%M')

#related to LCD with i2c backpack
import lcd_i2c
#check if i2c dev is attached
ADDR = str(hex(lcd_i2c.I2C_ADDR)).split("x")
OUTPUT = subprocess.check_output(['/usr/sbin/i2cdetect', '-y', '1'])
if ADDR[1] in OUTPUT.split(" "):
	i2c_dev = 1
else:
	i2c_dev = 0
#if no i2c dev, don't call lcd routines
#print "i2c_dev is", i2c_dev


def login_open_sheet(oauth_key_file, spreadsheet, worksheet_name):
	"""Connect to Google Docs spreadsheet and return the specified worksheet."""
	try:
		json_key = json.load(open(oauth_key_file))
		credentials = ServiceAccountCredentials.from_json_keyfile_name(GDOCS_OAUTH_JSON, SCOPES)
		gc = gspread.authorize(credentials)
		spreadsheet = gc.open(spreadsheet)
		worksheet = spreadsheet.worksheet(worksheet_name)
		return worksheet
	except Exception as ex:
		#print 'Unable to login and get spreadsheet.  Check OAuth credentials, spreadsheet name, and make sure spreadsheet is shared to the client_email address in the OAuth .json file!'
		#print 'Google sheet login failed with error:', ex
		#sys.exit(1)
		pass


#running an external program
#arg = "argument"
#exe = "/home/pi/executable"
#OUTPUT = subprocess.check_output([exe,arg])

#output_list = OUTPUT.split(" ")	#expecting space delimited text output without '\n'
#print "datetime value {}".format(DATE)
#print "returned value {}".format(OUTPUT)
#print "returned value0 {}".format(output_list[0])
#print "returned value1 {}".format(output_list[1])

#return1 = output_list[0]
#return2 = output_list[1]

humidity, temp = Adafruit_DHT.read_retry(SENSOR, PIN)
if (i2c_dev):
	lcd_i2c.lcd_init()

if humidity is None and temp is None:
	print "D/T/H ,",DATE,",","Sensor error!",",","Sensor error!",","
	if (i2c_dev):
		lcd_i2c.lcd_string("Sensor error!",lcd_i2c.LCD_LINE_1)
	sys.exit()

#Floating point representation is formatted as XX.X
TEMP = "{0:.1f}".format(temp)
HUM = "{0:.1f}".format(humidity)
# Prints
print "D/T/H ,",DATE,",",TEMP,",",HUM,","
TEMP_LCD = "Temp: "+ TEMP +" *C"
HUM_LCD = "Hum: "+ HUM +" %"
if (i2c_dev):
	lcd_i2c.lcd_string(TEMP_LCD,lcd_i2c.LCD_LINE_1)
	lcd_i2c.lcd_string(HUM_LCD,lcd_i2c.LCD_LINE_2)

worksheet = None
worksheet = login_open_sheet(GDOCS_OAUTH_JSON, GDOCS_SPREADSHEET_NAME, GDOCS_WORKSHEET_NAME)

if worksheet is None:
	print "D/T/H ,",DATE,",",'Login error!',",",'Login error!',","
	if (i2c_dev):
		lcd_i2c.lcd_init()
		lcd_i2c.lcd_string("Login error!",lcd_i2c.LCD_LINE_1)
else:
	try:
		worksheet.append_row((DATE,TEMP,HUM))
	except:
		print 'Append error!'
