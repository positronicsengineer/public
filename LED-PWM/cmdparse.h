//Copyright (c) 2018 positronics.engineer at yandex dot com.
//Distributed under the terms of the MIT license.

#ifndef CMDPARSE_H
#define CMDPARSE_H

typedef struct
{
	int plog;
    int flog;
    char *fname;
    int fpwm;
    float tcolor;
} cmdargs;

void cmdargs_init(cmdargs *cmdargs);
void getopts(int argc, char** argv, cmdargs *cmdargs);
void usage(void);

#endif //CMDPARSE_H
