//Copyright (c) 2018 positronics.engineer at yandex dot com.
//Distributed under the terms of the MIT license.

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "cmdparse.h"

void cmdargs_init(cmdargs *cmdargs)
{
    cmdargs->plog = 1;
    cmdargs->flog = 0;
    cmdargs->fname = NULL;
    cmdargs->fpwm = 250;
    cmdargs->tcolor = 0.2;
}

void usage(void)
{
	printf("Usage: <exe-name> <options>, where options are:\n");
	printf("  --plog on or off, prints logs to stdout (default is off)\n");
	printf("  --flog <file-name>, prints logs to <file-name> (default is disabled)\n");
	printf("  --fpwm <integer-value> Hz, pseudo pwm frequency for LED driving (default is 250 Hz)\n");
	printf("  --tcolor <floating-value> seconds, delay time to change color of LEDs (ie 0.5, 1.1 etc, default is 0.2 sec)\n");
	printf("  --help or -h prints this help\n");
	exit(1);
}

void getopts(int argc, char** argv, cmdargs *cmdargs)
{
	int count = 1;
	while (count < argc)
	{
		if (strcmp(argv[count], "--plog") == 0)
        {
			if (++count < argc)
			{
				if (strcmp(argv[count], "on") == 0)
                    cmdargs->plog = 0;
                else if (strcmp(argv[count], "off") == 0)
                    cmdargs->plog = 1;
                else 
                    usage();
			}
			else
				usage();
		}

        else if (strcmp(argv[count], "--flog") == 0)
        {
            if (++count < argc)
            {
				cmdargs->fname = argv[count];
                cmdargs->flog = 1;
            }
			else
				usage();
        }

        else if (strcmp(argv[count], "--fpwm") == 0)
        {
        	if (++count < argc)
        	{
        		cmdargs->fpwm = atoi(argv[count]);
        	}
        	else
        		usage();
        }

        else if (strcmp(argv[count], "--tcolor") == 0)
        {
        	if (++count < argc)
        	{
        		cmdargs->tcolor = atof(argv[count]);
        	}
        	else
        		usage();
        }

        else if ((strcmp(argv[count], "--help") == 0)|(strcmp(argv[count], "-h") == 0))
        {
        	usage();
        }
        else
        {
            printf("Invalid option!!!\n");
        	usage();
        }
        count++;
	}
}
