// Tables and functions are based on
// http://www.instructables.com/id/How-to-Make-Proper-Rainbow-and-Random-Colors-With-/
// It is release by CC BY-NC-SA (https://creativecommons.org/licenses/by-nc-sa/2.5/)
// Copyright (c) 2017 positronics.engineer at yandex dot com.
// Sources are modified and merged on purpose, May-2018

//Only put includes in a header if the header itself needs them.
//Otherwise include in source (*.c) file

#ifndef LOOKUP_H_   /* Include guard */
#define LOOKUP_H_

#include <stdint.h>

extern uint8_t Rval, Gval, Bval;

const uint8_t lights[360];
const uint8_t HSVlights[61];
const uint8_t HSVpower[121];

void setRGBpoint(uint8_t LED, uint8_t red, uint8_t green, uint8_t blue);

// the real HSV rainbow
void trueHSV(uint8_t LED, int angle);

// the 'power-conscious' HSV rainbow
void powerHSV(uint8_t LED, int angle);

// sine wave rainbow
void sineLED(uint8_t LED, int angle);

//update delay time to change color of LEDs
void setupdate(float tcolor);

void setup();
void loop();

#endif //LOOKUP_H_
