//Copyright (c) 2018 positronics.engineer at yandex dot com.
//Distributed under the terms of the MIT license.

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h> 
#include <unistd.h>
#include <stdint.h>
#include <sched.h>
#include <bcm2835.h> //http://www.airspayce.com/mikem/bcm2835/
#include "lookup.h"
#include "cmdparse.h"
#include "log.h" //https://github.com/rxi/log.c

#define R_PIN RPI_GPIO_P1_11
#define G_PIN RPI_GPIO_P1_16
#define B_PIN RPI_GPIO_P1_15

int Fpwm = 250; //Pseudo PWM frequency, Hz

void loglocker(void *udata, int lock)
{
	if (lock)
	{
		pthread_mutex_lock(udata);
	}
	else
	{
		pthread_mutex_unlock(udata);
	}
}


void *pseudopwm()
{
	uint8_t ret,r=0,g=0,b=0;
	pthread_t this_thread = pthread_self();
	struct sched_param params;
	params.sched_priority = sched_get_priority_max(SCHED_FIFO);
	//set thread priority to RT-linux
	ret = pthread_setschedparam(this_thread, SCHED_FIFO, &params);
	if (ret != 0)
	{
		// Print the error
		log_warn("Couldn't set priority, things may go wrong");
	}

	// Now verify the change in thread priority
	int policy = 0;
	ret = pthread_getschedparam(this_thread, &policy, &params);
	if (policy != SCHED_FIFO)
	{
		log_warn("Policy is not SCHED_FIFO");
	}

	while (1)
	{

		if(r < Rval)
		{
			bcm2835_gpio_set(R_PIN);
		}
		else
		{
			bcm2835_gpio_clr(R_PIN);
		}

		if(g < Gval)
		{
			bcm2835_gpio_set(G_PIN);
		}
		else
		{
			bcm2835_gpio_clr(G_PIN);
		}

		if(b < Bval)
		{
			bcm2835_gpio_set(B_PIN);
		}
		else
		{
			bcm2835_gpio_clr(B_PIN);
		}

		r++;
		g++;
		b++;
		//log_trace("r %d, g %d, b %d",r,g,b);
		usleep(1000000/(Fpwm*256));
		//256 is the number of steps (PWM resolution)
	}
}

void *RGBval()
{

	while(1)
        {
        	loop();
			//sleep(5);
        }

}

int main(int argc, char** argv)
{
        cmdargs options;
        cmdargs_init(&options);
        getopts(argc, argv, &options);

        pthread_t tr, tw;
        pthread_mutex_t loglock= PTHREAD_MUTEX_INITIALIZER;
        FILE *fp;
        log_set_quiet(options.plog);
        
        if (options.flog)
        {
            fp = fopen(options.fname, "w+");
            log_set_fp(fp);
        }
        
        //set lock function for multi-thread operation
        log_set_udata(&loglock);
        log_set_lock(loglocker);
        
        if (!bcm2835_init())
            return 1;
        
        Fpwm = options.fpwm;
        log_trace("Value of pseudo PWM is %d Hz",Fpwm);
        setupdate(options.tcolor);

        log_trace("Set ports");
        // Set PIN to be an output with a pull-up
        bcm2835_gpio_fsel(R_PIN, BCM2835_GPIO_FSEL_OUTP);
        bcm2835_gpio_set_pud(R_PIN, BCM2835_GPIO_PUD_UP);
        bcm2835_gpio_write(R_PIN, LOW);
        
        bcm2835_gpio_fsel(G_PIN, BCM2835_GPIO_FSEL_OUTP);
        bcm2835_gpio_set_pud(G_PIN, BCM2835_GPIO_PUD_UP);
        bcm2835_gpio_write(G_PIN, LOW);
        
        bcm2835_gpio_fsel(B_PIN, BCM2835_GPIO_FSEL_OUTP);
        bcm2835_gpio_set_pud(B_PIN, BCM2835_GPIO_PUD_UP);
        bcm2835_gpio_write(B_PIN, LOW);
        sleep(3);

        log_trace("Test each color");
        log_trace("RED LEDs on");
        bcm2835_gpio_write(R_PIN, HIGH);
        sleep(1);
        bcm2835_gpio_write(R_PIN, LOW);

        log_trace("GREEN LEDs on");
        bcm2835_gpio_write(G_PIN, HIGH);
        sleep(1);
        bcm2835_gpio_write(G_PIN, LOW);

        log_trace("BLUE LEDs on");
        bcm2835_gpio_write(B_PIN, HIGH);
        sleep(1);
        bcm2835_gpio_write(B_PIN, LOW);

        log_trace("Start threads");
        pthread_create(&tr,NULL,pseudopwm,NULL);
        pthread_create(&tw,NULL,RGBval,NULL);

        pthread_join(tr,NULL);
        pthread_join(tw,NULL);
        return 0;
}
