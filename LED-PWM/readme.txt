pi@raspberrypi ~ $ sudo ./led-pwm-rpi --help
Usage: <exe-name> <options>, where options are:
  --plog on or off, prints logs to stdout (default is off)
  --flog <file-name>, prints logs to <file-name> (default is disabled)
  --fpwm <integer-value> Hz, pseudo pwm frequency for LED driving (default is 250 Hz)
  --tcolor <floating-value> seconds, delay time to change color of LEDs (ie 0.5, 1.1 etc, default is 0.2 sec)
  --help or -h prints this help