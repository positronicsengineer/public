//Copyright (c) 2017 -2018 positronics.engineer at yandex dot com.
//Distributed under the terms of the MIT license.

#ifndef MYWEB_H
#define MYWEB_H

#include <QWidget>

//add network to *.pro file
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

namespace Ui {
class MyWeb;
}

class MyWeb : public QWidget
{
    Q_OBJECT

public:
    explicit MyWeb(QWidget *parent = 0);
    ~MyWeb();

private:
    Ui::MyWeb *ui;
    QString data, data_old;
    QString myurl;
    QString regex_string;
    QNetworkAccessManager nam;
    void ParseContent(void);
    void DisplayValue(void);
    void GetPage(QString url);
    void LoadSettings(void);
    int w_x, w_y;
    double w_opacity;
    bool taskbarico;

private slots:
    void Done(QNetworkReply * reply);
    void GetUpdate(void);

};

#endif // MYWEB_H
