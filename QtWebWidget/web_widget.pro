#-------------------------------------------------
#
# Project created by QtCreator 2017-09-09T17:48:21
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = web_widget
TEMPLATE = app


SOURCES += main.cpp\
        myweb.cpp

HEADERS  += myweb.h

FORMS    += myweb.ui

#to set icon for exe, uncomment below and put your ico file to build tree
#RC_ICONS += widget.ico
