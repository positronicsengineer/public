//Copyright (c) 2017 - 2018 positronics.engineer at yandex dot com.
//Distributed under the terms of the MIT license.

#include "myweb.h"
#include "ui_myweb.h"

#include <QDebug>
#include <QTimer>
#include <QFile>
#include <QRegExp>
#include <QDesktopWidget>
#include <QSettings>

//const QString myurl = "http://finance.google.com/finance?q=CURRENCY:USDTRY";
const int update_freq = 60000; //millisecond
//const QString regex_string = "1 USD = <span class=bld>(\\d+\\D\\d+)";
const double opacity = 0.6;   //default opacity
const QString config_file = "myconfig.ini";

MyWeb::MyWeb(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MyWeb)
{
    ui->setupUi(this);
    //widget background color, this can be changed by qtcreator ui designer, too.
    QPalette pal = palette();
    pal.setColor(QPalette::Background, "#f0f0f0");
    this->setAutoFillBackground(true);
    this->setPalette(pal);

    LoadSettings();
    if(taskbarico)
        this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint|Qt::WindowTransparentForInput|Qt::WindowStaysOnTopHint);
    else
        this->setWindowFlags(Qt::Window | Qt::FramelessWindowHint|Qt::WindowTransparentForInput|Qt::WindowStaysOnTopHint|Qt::Tool);
    this->setProperty("windowOpacity", w_opacity);
    //this->setAttribute(Qt::WA_TranslucentBackground,true);    //transparent back

    //assign a default location at top right
    QRect scr =QApplication::desktop()->screenGeometry();
    QPoint xy = scr.topRight();
    qDebug()<<"desktop top right x"<<xy.x()<<"desktop top right y"<<xy.y();
    qDebug()<<"widget w"<<this->width()<<"widget h"<<this->height();
    if(!scr.contains(w_x,w_y))
    {
        w_x = xy.x()-2*this->width();
        w_y = xy.y();
        qDebug()<<"Widget position in ini file is not within the desktop region, assign default!";
    }
    this->move(w_x,w_y);

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(GetUpdate()));
    connect(&nam , SIGNAL(finished(QNetworkReply*)) ,this, SLOT(Done(QNetworkReply*)));

    timer->start(update_freq);
}

MyWeb::~MyWeb()
{
    delete ui;
}


void MyWeb::GetUpdate(void)
{
    nam.get (QNetworkRequest(QUrl(myurl)));
}

void MyWeb::GetPage(QString url)
{
    qDebug()<<"get page";
    nam.get (QNetworkRequest(QUrl(url)));
}

void MyWeb::Done(QNetworkReply * reply)
{
    if(reply->error())
    {
        qDebug() << "ERROR!";
        qDebug() << reply->errorString();
    }
    else
    {
        qDebug() << reply->header(QNetworkRequest::ContentTypeHeader).toString();
        qDebug() << reply->header(QNetworkRequest::LastModifiedHeader).toDateTime().toString();;
        qDebug() << reply->header(QNetworkRequest::ContentLengthHeader).toULongLong();
        qDebug() << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        qDebug() << reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
        //data = QString(reply->readAll());

        //store web page content in a file
		QFile *file = new QFile("downloaded.txt");
        if(file->open(QFile::WriteOnly))
        {
            file->write(reply->readAll());
            file->flush();
            file->close();
        }
        delete file;
    }
    ParseContent();
    DisplayValue();
    reply->deleteLater();
}

void MyWeb::ParseContent(void)
{
    data_old = data;
    QFile rfile;
    rfile.setFileName("downloaded.txt");
    rfile.open(QFile::ReadOnly);
    QRegExp rate_search(regex_string);
    QString Line;
    QTextStream ContentStream(&rfile);

    qDebug()<<"start streaming";
    while(!ContentStream.atEnd())
    {
        //read line
		//here assumed your search item is in a line, you may need to review regex
        Line = ContentStream.readLine();
        if(rate_search.indexIn(Line,0) != -1)
        {
            data = rate_search.cap(1);
            //replace , with .
            if (data.contains(","))
                data.replace(",",".");
            qDebug()<<"captured"<<data;
        }
    }
}

void MyWeb::DisplayValue(void)
{
    if (data.toFloat()<data_old.toFloat())
        ui->value->setStyleSheet("color: red");
    else if (data.toFloat()>data_old.toFloat())
        ui->value->setStyleSheet("color: green");
    else
        ui->value->setStyleSheet("color: black");

    ui->value->setText(data);
}


void MyWeb::LoadSettings(void)
{
    QSettings settings(config_file, QSettings::IniFormat);
    settings.beginGroup("settings");
    myurl = settings.value("url").toString();
    qDebug()<<"url in ini file "<<myurl;
    regex_string = settings.value("regex").toString();
    qDebug()<<"regex in ini file "<<regex_string;
    w_opacity = settings.value("opacity", opacity).toDouble();
    qDebug()<<"opacitiy in ini file "<<w_opacity;
    w_x = settings.value("posx").toInt();
    qDebug()<<"posx in ini file "<<w_x;
    w_y = settings.value("posy").toInt();
    qDebug()<<"posy in ini file "<<w_y;
    taskbarico = settings.value("taskbaricon", true).toBool();
    qDebug()<<"taskbaricon is "<<taskbarico;
    settings.endGroup();
}
