help for config ini file
-----------------------------------
url is the web page that will be connected to get data
url = "http://finance.google.com/finance?q=CURRENCY:USDTRY"
-----------------------------------
regex is the regular expression which will be searched in url
"\" character must be escaped, ie "\\"
regex = "1 USD = <span class=bld>(\\d+\\D\\d+)"
-----------------------------------
posx is the x coordiante for widget
posy is the y coordiante for widget
top left of screen is (0,0), if (posx,posy) out of screen area, a default position at top right is assigned
ie;
posx = 1550
posy = 0
-----------------------------------
opacity must be [0,1], if not defined in inifile, it is 0.6 by default
ie;
opacity = 0.5
-----------------------------------
taskbaricon can "true" or "false", if not defined in inifile, it is true by default. 
When set to true, enables a window icon in Windows taskbar, otherwise no taskbar icon, force to shut down from task manager, look for web_widget.exe
ie; 
taskbaricon = false